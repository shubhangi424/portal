package com.java.demo;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Integer>{
	
	List<User> findByusername(String username);
	List<User> findByEmail(String email);
	List<User> findByemail(String email);
	

}
